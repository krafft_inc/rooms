define('views/HeaderView',['backbone', 'jquery', 'underscore', 'views/Templates','views/BaseView'],
    function(Backbone, $, _, Templates, BaseView){
        var _super = BaseView.prototype;

        var HeaderView = BaseView.extend({
            tpl:Templates.roomHeader,
            initialize:function(options){
                _super.initialize.apply(this, arguments);
            }
        });

        return HeaderView;

    });
