define(['backbone', 'jquery', 'underscore', 'views/Templates', 'models/OfficeCollection',
        'app/localStorage', 'views/BaseView', 'views/HeaderView', 'models/HeaderModel', 'app/controller'],
    function(Backbone, $, _, Templates, Offices, Storage, BaseView, HeaderView, HeaderModel, controller){

   var _super = BaseView.prototype;

   var OfficeView =  BaseView.extend({
       collection:Offices,
       tpl:Templates.office,
       events:{
         'click .office':'getOffice',
         'click .sort':'sortOffices'
       },
       restoredProperty:'id',
       initialize:function(){
           _.bindAll(this, 'render', 'getOffice', 'sortOffices');
           var self = this;
           this.controller = controller.getInstance();
           this.storage = Storage.getInstance();

           this.setElement($("#office-holder"));
           this.collection = new this.collection();
           this.collection.fetch();
           this.sorted = false;

           this.on('update', this.render, this);

           this.listenTo(this.collection, 'sync', this.render);

           _super.initialize.apply(this, arguments);

           this.headerModel = new HeaderModel({selected:false});

           this.header = new HeaderView({
               model : this.headerModel
           });

       },
       render:function(){

           var self = this;

           this.$el.empty();

           self.$el.prepend(this.header.render().el);

           this.collection.each(function(model){
               self.$el.append(_.template(self.tpl({model:model.toJSON()})));
           });


           $('#room-holder').hide();
           return this;
       },
       getOffice:function(ev){
           var id = this.$('.office-container input:checked').attr('data');
           this.collection.refreshCollection();
           this.collection.findWhere({id:id}).set('selected', true);
           this.office = this.collection.findWhere({id:id}).get('name');
           this.headerModel.set('office', this.office);

           this.storage.set({id:id});
           this.storage.save();
           this.render();
           router.navigate('/offices/'+ this.headerModel.get('office'));
           this.controller.getOffice(this.office);

       },
       sortOffices:function(){

           if(!this.sorted){
               this.collection.models = this.collection.sortBy(function(model){
                   return model.get('country');
               });
               this.sorted = true;
           }else{
               this.collection.models = this.collection.sortBy(function(model){
                   return model.get('name');
               });
               this.sorted = false;
           }
           this.trigger('update');
       }
   });

    return OfficeView;

});