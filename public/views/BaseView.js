define(['backbone', 'jquery', 'underscore'], function(Backbone, $, _){
        var BaseView =  Backbone.View.extend({
            initialize:function(options){
                _.bindAll(this, 'render', 'addChildren', 'remove', 'restoreData');
                this.options = options ||{};
                this.subViews = [];
                this.listenTo(this.collection, 'sync', this.restoreData);
                return this;
            },

            render:function(){
                var self = this;
                if(this.subViews){
                    _.each(this.subViews, function(view){
                        self.$el.append(view)
                    });
                }
                this.$el.html( _.template(this.tpl({model:this.options.model.toJSON()})));
                return this;
            },
            //It's a good practice to have set of children views in parent view,
            // it can save time for child view initialization
            addChildren:function(subview){
                this.subViews.push(subview);
            },
            //Util functions to avoid memory leaks,
            //overwrite of backbone native function to support nested views
            remove:function(){
                if(this.subViews){
                    _.each(this.subViews, function(subview){
                        subview.remove();
                    })
                }
                Backbone.View.prototype.remove.apply(this, arguments);
            },
            restoreData:function(){
                if(this.empty)return;
                var self = this;
                var id;

                this.storage.load(function(data){
                    if(!data)return;
                    id = data[self.restoredProperty];
                });

                this.collection.each(function(model){
                    if(!_.isArray(id) && model.get(self.restoredProperty) == id){
                        model.set('selected', true);
                    }else if(_.isArray(id) && (_.contains(id, model.get(self.restoredProperty)) ||
                        _.contains(id, model.get('id'))) ){
                        model.set('selected', true);
                    }
                });

                this.render();
            },
            isActive:function(){
                return this.collection.filter(function(model){
                    console.log(model.get('selected'));
                    return model.get('selected') ==true;
                }).length;
            },
        });

        return BaseView;

    });