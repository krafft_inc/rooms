define( ['underscore',
        "template!office",
        "template!room",
        "template!floor",
        "template!roomHeader"
    ],
    function (_, Office, Room, Floor, roomHeader) {
        var Templates = {
            office: Office,
            room: Room,
            floor: Floor,
            roomHeader:roomHeader
        };
        return Templates;

    });
