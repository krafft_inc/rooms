define('FloorView',['backbone', 'jquery', 'underscore', 'views/Templates','views/BaseView'],
    function(Backbone, $, _, Templates, BaseView){
        var _super = BaseView.prototype;
        var FloorView = BaseView.extend({
            empty:true,
            tpl:Templates.floor,
            initialize:function(options){
                _.bindAll(this,  'fetchQuantity');
                this.fetchQuantity();
                _super.initialize.apply(this, arguments);

            },
            fetchQuantity:function(){
                var floorID = this.model.get('floorId');
                var quantity =  this.collection.filter(function(room){
                    return room.get('floorId') == floorID;
                }).length;
                this.model.set('quantity', quantity);
                return quantity;
            }
        });

        return FloorView;

    });

define('views/FloorsView',['backbone', 'jquery', 'underscore', 'views/Templates', 'models/FloorsCollection',
        'FloorView', 'views/RoomsView',  'models/RoomsCollection', 'app/localStorage', 'views/BaseView',
        'views/HeaderView', 'models/HeaderModel',],
    function(Backbone, $, _, Templates, FloorsModel, FloorView, RoomsView, RoomsModel, Storage, BaseView, HeaderView, HeaderModel){
        var _super = BaseView.prototype;
        var FloorView =  BaseView.extend({
            restoredProperty:'floorId',
            collection:FloorsModel,
            view:FloorView,
            roomsView:RoomsView,
            events:{
              'click .floors':'getRooms',
                'click .sort':'sortFloors'
            },
            initialize:function(options){
                var self = this;
                this.storage = Storage.getInstance();
                _.bindAll(this, 'render', 'getRooms', 'sortFloors', 'getCurrentFloors');
                this.setElement($("#floor-holder"));
                this.floors = this.collection = new this.collection();
                this.floors.fetch();

                this.sorted  =false;
                this.rooms   =  new RoomsModel();

                this.rooms.fetch({success:function(){

                    self.roomView = new RoomsView({
                        collection: self.rooms
                    });
                    self.render();
                }});
                this.on('update', this.render, this);

                this.headerModel = new HeaderModel({selected:false});

                this.header = new HeaderView({
                    model : this.headerModel
                });

                _super.initialize.apply(this, arguments);
            },
            render:function(country_id, office){
                var self = this;
                if(office)
                    this.headerModel.set('office', office);
                this.country_id = country_id || this.country_id;

                var floors = this.getCurrentFloors();

                this.$el.empty();

                this.$el.prepend(this.header.render().el);

                _.each(floors, function(floor){
                    self.$el.append(new self.view({
                        model:floor,
                        collection:self.rooms
                    }).render().el)
                });
                if(this.$('.floor-container input:checked').size()){
                    $('#room-holder').show();

                }
                this.getRooms();
            },
            getCurrentFloors:function(){
                var self = this;
                return this.floors.filter(function(model){
                    return model.get('officeId') == self.country_id;
                });
            },
            getRooms:function(){
                var ids = [];
                var self = this;
                var floors = this.getCurrentFloors();

                 $.each(this.$('.floor-container input:checked'), function(index,el){
                    ids.push($(el).attr('data'));
                });

                floors.map(function(model){
                    if(_.contains(ids, model.get('floorId'))){
                        model.set('selected',true);
                    }else{
                        model.set('selected',false);
                    }
                });

                if(!ids.length){
                    $('#room-holder').hide();
                }else{
                    $('#room-holder').show();
                    this.storage.set({floorId:ids});
                    this.storage.save();
                }

                this.$('.room-header').detach();

                this.floor = floors.length == ids.length? 'All': (ids.length||0);
                this.headerModel.set('floor', this.floor);
                this.$el.prepend(this.header.render().el);

                if(this.roomView && ids)
                    this.roomsViewInstance = this.roomView.render(ids, this.headerModel.get('office'), this.floor);
            },
            sortFloors:function(){
                if(!this.sorted){
                    this.floors.models = this.floors.sortBy(function(model){
                        return model.get('quantity')
                    });
                    this.sorted = true;
                }else{
                    this.floors.models = this.floors.sortBy(function(model){
                        return parseFloat(model.get('name'));
                    });
                    this.sorted = false;
                }
                this.headerModel.set('sorted', this.sorted);
                this.trigger('update');
            }
        });
        return FloorView;

    });