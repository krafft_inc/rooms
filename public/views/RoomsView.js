define('RoomView',['backbone', 'jquery', 'underscore', 'views/Templates','views/BaseView'],
    function(Backbone, $, _, Templates, BaseView){
        var _super = BaseView.prototype;
        var RoomView = BaseView.extend({
            tpl:Templates.room,
            initialize:function(options){
                _super.initialize.apply(this, arguments);
            }
        });

        return RoomView;

    });

define('views/RoomsView',['backbone', 'jquery', 'underscore', 'views/Templates', 'RoomView',
    'views/HeaderView', 'models/HeaderModel','views/BaseView', 'app/localStorage'],
    function(Backbone, $, _, Templates, RoomView, HeaderView, HeaderModel, BaseView, Storage){
        var _super = BaseView.prototype;
        var RoomsView =  BaseView.extend({
            restoredProperty:'roomId',
            events:{
              'click .sort': 'sortRooms',
                'click .rooms':'checkRooms'
            },
            initialize:function(options){
                this.rooms = this.collection = options.collection;
                this.storage = Storage.getInstance();
                _.bindAll(this, 'render', 'sortRooms', 'getCurrentRooms', 'checkRooms');
                this.sorted = false;
                this.setElement($("#room-holder"));
                this.on('update', this.render, this);
                this.headerModel = new HeaderModel({selected:false});

                this.header = new HeaderView({
                    model : this.headerModel
                });
                _super.initialize.apply(this, arguments);
            },
            render:function(floor_ids, office, floor){
                this.floor_ids = floor_ids|| this.floor_ids;
                this.office = office|| this.office;
                this.floor = floor|| this.floor;
                this.headerModel.set('office', this.office);
                this.headerModel.set('floor', this.floor);
                var self = this;
                var rooms = this.getCurrentRooms();
                this.$el.empty();

                _.each(rooms, function(room){
                    self.$el.append(new RoomView({
                        model:room
                    }).render().el)
                });
                this.room = this.$('.room-container input:checked').size() ||0;
                this.headerModel.set('room', this.room);

                this.$el.prepend(this.header.render().el);
                return this;
            },
            sortRooms:function(){
                if(!this.sorted){
                    this.rooms.models = this.rooms.sortBy(function(model){
                        return model.get('personsCapacity')
                    });
                    this.sorted = true;
                }else{
                    this.rooms.models = this.rooms.sortBy(function(model){
                        return parseFloat(model.get('name'));
                    });
                    this.sorted = false;
                }
                this.headerModel.set('sorted', this.sorted);
                this.trigger('update');
            },
            getCurrentRooms:function(){
                var self = this;
                return  this.rooms.filter(function(model){
                    return _.contains(self.floor_ids, model.get('floorId'));
                });

            },
            checkRooms:function(){
                var ids = [];
                var self = this;

                var rooms = this.getCurrentRooms();

                $.each(this.$('.room-container input:checked'), function(index,el){
                    ids.push($(el).attr('data'));
                });

                rooms.map(function(model){
                    if(_.contains(ids, model.get('id'))){
                        model.set('selected',true);
                    }else{
                        model.set('selected',false);
                    }
                });
                if(ids.length){
                    this.storage.set({roomId:ids});
                    this.storage.save();
                }

                this.$('.room-header').empty();
                this.room = rooms.length == ids.length? 'All': (ids.length||0);
                this.headerModel.set('room', this.room);
                this.$el.prepend(this.header.render().el);
            }

        });

        return RoomsView;

    });