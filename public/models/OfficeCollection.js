define('models/OfficeCollection',['backbone', 'underscore', 'models/BaseCollection'], function (Backbone, _, BaseCollection) {

    var _super = BaseCollection.prototype;

    var officeCollection = BaseCollection.extend({
        initialize: function (attr, options) {
            this.options = options;
            _super.initialize.apply(this, arguments);
        },
        url:function(){
            return '/offices'
        }
    });
    return officeCollection;
});