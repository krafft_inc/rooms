define('models/BaseCollection',['backbone', 'underscore', 'models/BaseModel'], function (Backbone, _, BaseModel) {

    var BaseCollection = Backbone.Collection.extend({
        model:BaseModel,
        initialize: function (attr, options) {
            this.options = options;
            _.bindAll(this, 'refreshCollection', 'setEmbeddedModel');
        },
        sync : function(method, model, options) {
            options.timeout = 10000;
            options.dataType = 'json';
            return Backbone.sync(method, model, options);
        },
        parse : function(data) {
            var models = [];
            var self = this;
            _.each(data, function(model){
                if(model._embedded)
                    model = self.setEmbeddedModel(model);
                models.push(model);
            });

            return _.sortBy(models, function(model){
                if(_.isNumber(model.name))
                     return parseFloat(model.name);
                return model.name;
            });
        },
        refreshCollection:function(){
            this.each(function(model){
                model.set('selected', false);
            })
        },
        setEmbeddedModel:function(model){

            var embeddedName = _.keys(model._embedded).length == 1? _.first(_.keys(model._embedded)) :null;

            if(!embeddedName){
                console.error('Unable to parse embedded model');
                throw new Error();
            }
            var embeddedKeys = {};
             _.chain(model._embedded[embeddedName]).keys().map(function(key){
                 embeddedKeys[key] = model._embedded[embeddedName][key];
            });

            model.embeddedModel = new BaseModel(_.defaults({
                embeddedName: embeddedName,
                embeddedProperties: _.keys(model._embedded[embeddedName]).join(',')
                //we need to save embedded properties as string to avoid changes
                //if we pass object or array it might be changed by link
            }, embeddedKeys));
            return model
        }
    });
    return BaseCollection;
});