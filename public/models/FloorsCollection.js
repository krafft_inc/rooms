define('models/FloorsCollection',['backbone', 'underscore', 'models/BaseCollection'], function (Backbone, _, BaseCollection) {

    var _super = BaseCollection.prototype;

    var FloorCollection = BaseCollection.extend({
        initialize: function (attr, options) {
            this.options = options;
            _super.initialize.apply(this, arguments);
        },
        url:function(){
            return '/floors'
        }
    });
    return FloorCollection;
});
