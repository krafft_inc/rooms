define(['backbone', 'underscore', 'models/BaseModel'], function (Backbone, _, BaseModel) {

    var HeaderModel = BaseModel.extend({
        defaults: {
            'office':'',
            'floor':'',
            'room':'',
            'sorted':''
        },
        initialize: function (attr, options) {
            this.options = options;
        },
    });
    return HeaderModel;
});

