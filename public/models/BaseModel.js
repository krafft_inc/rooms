define(['backbone', 'underscore'], function (Backbone, _) {

    var BaseModel = Backbone.Model.extend({
        defaults: {
            'selected':false,
            'embeddedModel':'',
            'embeddedProperties':'',
            'embeddedName':''
        },
        initialize: function (attr, options) {
            this.options = options;
            _.bindAll(this, 'set', 'get', 'genericMethod', 'toJSON');
        },
        get:function(){
            //TODO convert get method to genericMethod call
            //Overwrite model.get function in order to support embedded model
            //If model has embedded model &&
            //Embedded model has needed property &&
            //Parent model doesn't have it
            //It'll take property from embedded model
            //So I can reach country property which is located _embedded.location.country
            //with call get('country') on main model
            if(_.isArray(_.first(arguments)))
                arguments = _.first(arguments);

            if(this.attributes.embeddedModel && this.attributes.embeddedModel.get(_.first(arguments))
                && ! Backbone.Model.prototype.get.apply(this, arguments)){

                return Backbone.Model.prototype.get.apply(this.attributes.embeddedModel, [_.first(arguments)]);
            }

          return  Backbone.Model.prototype.get.apply(this, arguments);
        },
        set:function(){
            //If it's initial set we pass it to real set method
            if(_.isObject(_.first(arguments))){

                return Backbone.Model.prototype.set.apply(this, arguments);
            }
            //If set is not initial, we can try to apply set method to embedded model
            //Using memoize method to cache function, so it won't be created every time model need it
            //using curring to prepend first argument to setter method
            var setter = _.memoize(_.bind(this.genericMethod, this).curry('set'));
            setter.apply(this, arguments)
        },
        genericMethod:function(method, property, value){
            if(_.isArray(property))
                property = property[1];
            //Get [].splice method in variable
            var splice = Array.prototype.slice;
            //Convert arguments pseudo array to array
            var args = splice.call(arguments);
            //Checking if property in base moddel
            if(this.attributes.embeddedModel &&
                _.contains(_.keys(this.attributes.embeddedModel.attributes), property)
                && ! _.contains(_.keys(this.attributes), property)){
                //Property wasn't found in base maethod applying set method on embedded model
                return Backbone.Model.prototype[method].apply(this.attributes.embeddedModel, _.rest(args));
            }
            return  Backbone.Model.prototype[method].apply(this, _.rest(args));
        },
        toJSON:function(){
            //Overwrite model.get function in order to support embedded model
            //It takes embedded models properties for templates
            var JSON = Backbone.Model.prototype.toJSON.apply(this, arguments);
            if(JSON.embeddedModel){
                var embeddedValues = this.restoreEmbeddedProperties(JSON.embeddedModel.toJSON());
                JSON = _.defaults(JSON, embeddedValues);
            }

            return JSON
        },
        restoreEmbeddedProperties:function(model){
            //Util method it returns only embedded properties from embedded model
            return _.pick(model, model.embeddedProperties.split(','))
        }
    });
    return BaseModel;
});