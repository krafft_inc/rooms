define(['backbone','jquery', 'app/localStorage'], function(Backbone, $, localStorage){

    var instance;
    function Controller (args){
        var self = this;
        this.showOffices =  function(){
            if ( !require.defined("views/OfficesView"))
                require(["views/OfficesView"], function (OfficesView) {

                    var officesView = new OfficesView();
                    self.store.load(function(data){
                        if(data.id){
                            self.getOffice();
                        }
                    });
                });
        };

        this.getOffice =function(office){

            this.showOffices();
            if ( !require.defined("views/FloorsView"))
                require(["views/FloorsView"], function (OfficesView) {

                    self.store.load(function(data){

                        self.officesView = new OfficesView({id:data.id, office:office});
                        self.officesView.render(data.id, office)
                    });
                });
            else
            self.store.load(function(data){
                self.officesView.render(data.id,office)
            });
        };
        this.initialize = function(){
            self.store = localStorage.getInstance({
                source:'local',
                name:'appStorage'
            });

        };

        this.initialize();
    }



    return {
        getInstance:function(){
            if(instance){
                return instance;
            }
            instance = new Controller();

            return instance;
        }
    };
});
