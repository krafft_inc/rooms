define(['backbone', 'underscore'], function(Backbone, _){

    var instance;

    function Store (options){
        var self = this;
        this.source = options.source || 'local';
        this.name = options.name;
        this.loaded = false;
        this.data = {};

        if (this.source == 'local' && !Store.isAvailable('local')) {
            throw "no local storage!";
        }

        this.load =function (callback, scope) {

            var store = localStorage.getItem(self.name);
            self.data = ( store && JSON.parse(store)) || {};
            self.loaded = true;
            callback.bind(scope)(self.data, self);
        };

        this.save = function () {
            localStorage.setItem(self.name, JSON.stringify(self.data));

        };

        this.get = function () {
            if (!self.loaded) {
                return null;
            }
            return self.data;
        };


        this.set = function (value) {
            if (!self.loaded && !_.isObject(value)) {
               self.data = {};
                return false;
            }
            self.data = _.defaults(value, self.data);
        };

        this.reset = function () {
            self.set(null);
            self.save();
        }
    }

    Store.isAvailable = function (source) {
        if(!source) source = 'local';
        if(source == 'local' && !localStorage) {
            return false;
        }
        return true;
    };


    return {
        getInstance:function(options){
            if(instance){
                return instance;
            }
            instance = new Store(options);

            return instance;
        }
    };
});
