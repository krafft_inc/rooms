define(['backbone', 'jquery', 'underscore', 'app/controller'], function(Backbone, $, _, controller){

    var Controller = controller.getInstance();

    var router = Backbone.Router.extend({
        initialize:function(){
            this.route(/^/, 'index');
            this.route(/^offices\/(\D+)/, 'offices');
        },
        index:function(){
            Controller.showOffices()
        },
        offices:function(office){
            Controller.getOffice(office)
        }
    });


    return router;
});