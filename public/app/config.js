'use strict';

require.config({
    baseUrl:'/',
    waitSeconds: 150,
    lodashLoader: {
        ext: ".jshtml",
        root: "/../templates/",
        mimeType: "application/json"
    },
    shim:{
        "jquery": {
            exports: "jquery"
        },
        underscore:{
            exports: "underscore"
        },
        backbone:{
            exports:'backbone',
            deps:['jquery', 'underscore']
        },
        "lodash": {
            exports: "_"
        },
        "templates": {
            deps: ["lodash"]
        },
    },
    paths:{
        jquery:'/bower_components/jquery/dist/jquery',
        underscore:'/bower_components/underscore/underscore',
        backbone:'/bower_components/backbone/backbone',
        "lodash": "/bower_components/lodash//lodash",
        "text": "/bower_components/text",
        "async": '/bower_components/requirejs-plugins/src/async',
        "promise": "/bower_components/requirejs-plugins/src/requirejs-promise",
        "template": "/bower_components/requirejs-plugins/src/loader",
    }
});

require(['backbone', 'app/router'], function(Backbone, Router){

    Function.prototype.curry = function () {
        var slice = Array.prototype.slice,
            args = slice.apply(arguments),
            that = this;
        return function () {
            return that.apply(null, args.concat(slice.apply(arguments)));
        };
    };

    window.router = new Router();
    Backbone.history.start({
        pushState: false,
    });
});