var express = require('express');
var app = express();
var path = require('path');
var offices = require('./data/offices');
var floors = require('./data/floors');
var rooms = require('./data/rooms');

app.use(express.static (path.join(__dirname+'/public')));

app.get('/', function(req, res){
    res.render('index')
});

app.get('/offices', function(req, res){
    res.json(offices);
});

app.get('/floors', function(req, res){
    res.json(floors);
});
app.get('/rooms', function(req, res){
    res.json(rooms);
});

app.listen(3000, function(){
    console.log('Server is running');
});